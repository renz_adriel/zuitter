<?php
    session_start();
    session_unset();
    session_destroy();

    header("LOCATION: ../views/landing_page.php");
?>