<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Zuitter</title>
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <span><img src="../assets/zuitt.png" alt="" width="40px"></span>
        <a class="navbar-brand" href="#">Zuitter</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor02" aria-controls="navbarColor02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>


        <div class="collapse navbar-collapse" id="navbarColor02">
            <ul class="navbar-nav mr-auto">

                <?php
                session_start();
                // session_destroy();
                // die();
                // var_dump(isset($_SESSION['user']));
                // die();
                if (isset($_SESSION['user'])) {
                    ?>
                    <li class="nav-item active">
                        <a class="nav-link" href="../views/main.php">Freedom Page <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Hello, <?php echo $_SESSION['user']['firstname'] ?></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../controllers/process_logout.php">Logout</a>
                    </li>
                <?php
                } else {
                    ?>
                    <li class="nav-item">
                        <a class="nav-link" href="../views/login.php">Login</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../views/register.php">Register</a>
                    </li>
                <?php
                }

                ?>






            </ul>
        </div>
    </nav>

</body>

</html>