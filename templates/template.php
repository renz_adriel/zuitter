<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Zuitter</title>

    <!-- <link rel="stylesheet" href="https://bootswatch.com/4/sketchy/bootstrap.css"> -->
    <!-- <link rel="stylesheet" href="https://bootswatch.com/4/cerulean/bootstrap.css"> -->
    <link rel="stylesheet" href="https://bootswatch.com/4/lumen/bootstrap.css">
    <link rel="stylesheet" href="../assets/styles/style.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet"> 
</head>

<body>
    <?php require 'navbar.php' ?>
    <?php get_content() ?>
    <?php require 'footer.php' ?>
</body>

</html>