<?php
require "../templates/template.php";

function get_content()
{
	require "../controllers/connection.php";
	?>
	<h1 class="text-center py-5 display-4">Welcome to the Freedom Wall</h1>
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-2">
				<!--  -->
				<h3>Categories</h3>
				<ul class="list-group-border">
					<?php
						//call categories
						$categories_query = "SELECT * FROM categories";
						$categoryList = mysqli_query($conn, $categories_query);
						foreach ($categoryList as $indiv_category) {
							?>
						<li class="list-group-item">
							<a href="main.php?category_id=
                        <?php echo $indiv_category['id'] ?>">
								<?php echo $indiv_category['name'] ?></a>
						</li>
					<?php
						}
						?>
				</ul>
		

		</div>
		<div class="col-lg-8">
			<div class="row justify-content-center">
				<?php
					if (isset($_SESSION['user'])) {

						?>
					<div class="col-lg-10">
						<div class="card my-2">
							<form action="../controllers/process_post.php" method="POST">
								<div class="form-group">
									<label for="topicName">
										Topic Name:
									</label>
									<input type="text" name="topicName" class="form-control">
								</div>
								<div class="form-group">
									<label for="post">Post:</label>
									<textarea class="form-control" name="post"></textarea>
								</div>
								<div class="form-group">
									<label for="category_id">Category:</label>
									<br>
									<input type="radio" name="category_id" value="1" required> Freedom Wall
									<br>
									<input type="radio" name="category_id" value="2"> Education
									<br>
									<input type="radio" name="category_id" value="3"> Suggestion
									<br>
								</div>
								<div class="card-footer">
									<button class="btn btn-info">
										Submit
									</button>
								</div>
							</form>
						</div>
					</div>
					<?php
						}
						$posts_query = "SELECT posts.id,topic_name, post, image,user_id, category_id  FROM posts JOIN (users, categories) ON (posts.user_id = users.id AND posts.category_id = categories.id)";

	if (isset($_GET['category_id'])) {

		if ($_GET['category_id'] == $indiv_category['id']) {
			$catId = $_GET['category_id'];
			$posts_query .= " WHERE category_id = $catId";
		};
	};
						$posts = mysqli_query($conn, $posts_query);
						foreach ($posts as $indiv_post) {
							// var_dump($indiv_post);
							// die();
							$post_userid = $indiv_post['user_id'];
							$users = "SELECT * FROM users WHERE id = $post_userid";
							$users_info = mysqli_query($conn, $users);
							foreach ($users_info as $indiv_user) {
								?>
						<div class="col-lg-10">
							<div class="card my-2">
								<p style="font-size: 14px" class="card-text">u/<?php echo $indiv_user['username'] ?></p>

								<h3 class="card-text"><?php echo $indiv_post['topic_name'] ?></h3>
								<div class="card-body">
									<p class="card-text">
										<?php echo $indiv_post['post'] ?>
									</p>
								</div>
								<div class="card-footer">
									<form action="" method="POST">
										<button class="btn btn-success">comments</button>
										<a class="btn btn-danger" href="../controllers/process_delete.php?id=<?php echo $indiv_post['id']; ?> ">Delete</a>
									</form>
								</div>
								<div class="row justify-content-center">
									<?php
												$user_id = $indiv_user['id'];
												$post_id = $indiv_post['id'];
												$comments_query = "SELECT * FROM comments JOIN (users,posts) ON (comments.user_id = users.id AND comments.post_id = posts.id)";
												$comments_info = mysqli_query($conn, $comments_query);

												foreach ($comments_info as $indiv_comment) {
													// var_dump($indiv_comment['post_id']);
													if ($post_id == $indiv_comment['post_id']) {
														?>

											<div class="col-lg-10">
												<div class="card my-2">
													<p class="card-text">
														<?php echo $indiv_comment['comments'] ?>
													</p>

												</div>

											</div>
									<?php
													}
												}
												?>
								</div>
							</div>
						</div>
				<?php
						}
					}
					?>

			</div>
		</div>
		<div class="col-lg-2">

		</div>
	</div>
	</div>
<?php
}

?>