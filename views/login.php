<?php 
    require "../templates/template.php";

    function get_content() {
        ?>
        <h1 class="text-center py-5">Login</h1>

        <div class="col-lg-4 offset-lg-4">
            <form action="" method="POST">
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" name="email" id="email" class="form-control"> 
                    <span></span>
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" name="password" id="password" class="form-control"> 
                </div>
            </form>
            <button class="btn btn-info btn-opink" type="button" id="loginUser">Login</button><p>Not yet registered?
                <a href="register.php">Register</a>
            </p>
        </div>

        <script src="../assets/scripts/login.js" type="text/javascript"></script>
    <?php
    }
?>