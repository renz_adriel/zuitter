<?php
require "../templates/template.php";

function get_content()
{
    ?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Document</title>
        <link rel="stylesheet" href="../assets/styles/style.css">
        <link href="https://fonts.googleapis.com/css?family=Lora&display=swap" rel="stylesheet">


    </head>

    <body>

        <div class="vh-100 bg-info text-white d-flex flex-column justify-content-lg-center container-fluid">
            <h1 class="text-center text-size">ZUITTER</h1>
            <div class="col-lg-1 mx-auto justify-content-lg-center button-container">
                <button class="btn btn-opink btn-info my-2"><a href="login.php">Login</a></button>
                <button class="btn btn-opink btn-info my-2"><a href="register.php">Register</a></button>
            </div>
        </div>
    </body>



    </html>


<?php
}

?>