
loginUser.addEventListener("click", () => {
    
    let email = document.querySelector("#email").value;
    let password = document.querySelector("#password").value;

    let data = new FormData;

    data.append("email", email);
    data.append("password", password);

    console.log(email, password);
    fetch("../../controllers/process_login.php", {
        method: "POST",
        body:data
    }).then(response => {
        return response.text();
    }).then(data_from_fetch => {
        console.log(data_from_fetch);
        if (data_from_fetch == "login_failed") {
            
            document.querySelector("#email").nextElementSibling.innerHTML = "Please provide correct credentials";
        } else {
            window.location.replace("../../views/main.php");
        }
    })

})